// Dear emacs, this is -*-c++-*-
#ifndef CxAODMaker_ElectronHandler_WWW_H
#define CxAODMaker_ElectronHandler_WWW_H

// used to get IFF truth classification
#include "RegionClass/RegionClass.h"

#include "AsgTools/AnaToolHandle.h"
#include "CxAODMaker/ElectronHandler.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "ElectronPhotonSelectorTools/LikelihoodEnums.h"

class ElectronHandler_WWW : public ElectronHandler {
 public:
  ElectronHandler_WWW(const std::string& name, ConfigStore& config, xAOD::TEvent* event, EventInfoHandler& eventInfoHandler);

  virtual ~ElectronHandler_WWW();

  virtual EL::StatusCode initializeTools() override;

 protected:
  // JL: declare truth classifier object
  RegionClass* electronTruthClassifier;

  AsgElectronEfficiencyCorrectionTool* m_trigToolMediumLHIsoFixedCutLoose;
  AsgElectronEfficiencyCorrectionTool* m_trigEffToolMediumLHIsoFixedCutLoose;
  AsgElectronEfficiencyCorrectionTool* m_effToolIsoFixedCutLooseTightLH_PLV;

  // selection functions
  bool passVetoElectron(xAOD::Electron* electron);
  bool passZElectron(xAOD::Electron* electron);  // override the same function in CxAODMaker
  bool passWElectron(xAOD::Electron* electron);

  //Override function (calls base)
  virtual EL::StatusCode decorateOriginParticle(const xAOD::Electron* electron) override;
  virtual EL::StatusCode decorate(xAOD::Electron* electron) override;
  virtual EL::StatusCode calibrateCopies(xAOD::ElectronContainer* particles, const CP::SystematicSet& sysSet) override;
  virtual EL::StatusCode writeCustomVariables(xAOD::Electron* inPart, xAOD::Electron* outPart, bool isKinVar, bool isWeightVar,
                                              const TString& sysName) override;
};

#endif
