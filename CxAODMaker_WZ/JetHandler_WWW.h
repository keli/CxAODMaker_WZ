// Dear emacs, this is -*-c++-*-
#ifndef CxAODMaker_JetHandler_WWW_H
#define CxAODMaker_JetHandler_WWW_H

#include "CxAODMaker/JetHandler.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"

class JetHandler_WWW : public JetHandler {
 public:
  JetHandler_WWW(const std::string& name, ConfigStore& config, xAOD::TEvent* event, EventInfoHandler& eventInfoHandler);
  virtual ~JetHandler_WWW();

 protected:
  // selection functions
  bool passSignalJet(xAOD::Jet* jet);

  BTaggingSelectionTool* m_bTagTool;

  virtual EL::StatusCode initializeTools() override;
  virtual EL::StatusCode writeCustomVariables(xAOD::Jet* inPart, xAOD::Jet* outPart, bool isKinVar, bool isWeightVar,
                                              const TString& sysName) override;
};

#endif
