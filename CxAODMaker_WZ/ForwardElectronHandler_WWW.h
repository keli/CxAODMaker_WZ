// Dear emacs, this is -*-c++-*-
#ifndef CxAODMaker_ForwardElectronHandler_WWW_H
#define CxAODMaker_ForwardElectronHandler_WWW_H

#include "CxAODMaker/ForwardElectronHandler.h"

class ForwardElectronHandler_WWW : public ForwardElectronHandler {
 public:
  ForwardElectronHandler_WWW(const std::string& name, ConfigStore& config, xAOD::TEvent* event, EventInfoHandler& eventInfoHandler);

  virtual ~ForwardElectronHandler_WWW();

 protected:
  // selection functions
  bool passVetoElectron(xAOD::Electron* electron);

  virtual EL::StatusCode writeCustomVariables(xAOD::Electron* inElectron, xAOD::Electron* outElectron, bool isKinVar, bool isWeightVar,
                                              const TString& sysName) override;
};

#endif
