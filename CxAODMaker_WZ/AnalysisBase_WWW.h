#ifndef CxAODMaker_AnalysisBase_WWW_H
#define CxAODMaker_AnalysisBase_WWW_H
#include "CxAODMaker/AnalysisBase.h"

class AnalysisBase_WWW : public AnalysisBase {
 protected:
  virtual EL::StatusCode initializeSampleInfo() override;
  virtual EL::StatusCode initializeHandlers() override;
  virtual EL::StatusCode initializeSelection() override;

 public:
  AnalysisBase_WWW() : AnalysisBase() {}

  ~AnalysisBase_WWW() {}
  virtual EL::StatusCode setupJob(EL::Job& job) override;

  ClassDefOverride(AnalysisBase_WWW, 1);
};

#endif
