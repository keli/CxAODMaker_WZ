1. setup package: Source /afs/cern.ch/user/w/wasu/public/VBS/getWZ.sh origin/master WWWFramework_tag_master 0

2. in CxAODMaker_WZ/data/framework-run.cfg 
Change the corresponding “sample_in” ,“submitDir”, “ilumicalcFiles” and “configFiles”  according to the samples you are running. 
if the input is a txt file with grid in the name, the driver configuration will be overrided and use grid automatically.
Check Pileup settings depending on you are running data15+16 and mca  or data17 and mcd or data18 and mce , you need to uncomment the blocks in configuration file and comment out others.


3.Go to ‘run’ folder,do: WZframework  
	It will output CxAOD files


